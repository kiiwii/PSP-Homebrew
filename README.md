# PSP-Homebrew

This is homebrew for the PSP that I enjoy or want to archive. This repository is mainly for myself if I ever get another PSP and want to get all my homebrew back on it without going through a bunch of googling, though this repo is public for anyone that wants to get homebrew from here.

I did not create any of the homebrew in this repo, this is just a collection of homebrew :)

# List

This is the list of homebrew in alphabetical order and what exactly it is.

- `FILER.zip`: File Manager for the PSP, you can also dump games from this homebrew.
- `MasterBoy.zip`: Gameboy, Gameboy Color, Master System & Game Gear emulator.
- `NesterJ.zip`: NES emulator.
- `PicoDrive.zip`: Sega Genesis & Sega CD emulator.
- `pspdoomv1.4.zip`: Doom, but for the PSP. Comes with the shareware version of Doom so you can get up and running with the free version.
- `seplugins.zip`: This is all the plugins I use with my PSP, which are...
    - `cxmb`: custom xmb themes for 6.61
    - `prxshot`: Capture screenshots by pressing the ♪ key on the PSP
    - `UMDKiller`: Press the ♪ key with a UMD in the PSP and you can rip said UMD, only way to make reliable UMD Video dumps.
- `s9xTYLme_mod.zip`: SNES emulator.
- `TempGBA.zip`: GBA emulator.
- `UMDKiller.zip`: Rips UMD games, I use it over Filer since this has been more reliable from experience.